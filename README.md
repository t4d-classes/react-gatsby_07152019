# Welcome to React & Gatsby Class!

## Instructor

Eric Greene

## Schedule

Class:

- Monday - Wednesday: 9am to 5pm

Breaks:

- Morning Break: 10:25am to 10:35am
- Lunch: 12pm to 1pm
- Afternoon #1 Break: 2:15pm to 2:25pm
- Afternoon #2 Break: 3:40pm to 3:50pm

## Course Outline

### JavaScript content will be included as needed in the class

- Day 1 - Overview of React, JSX, Function Components, Props, Prop Types, State Hook, Events
- Day 2 - Composition, Containment & Specialization, Other Hooks, Fetch API
- Day 3 - Gatsby, Project Setup, Page and Layout Components, Data, GraphQL, Plugins, Deployment

## Links

### Instructor's Resources

- [Accelebrate](https://www.accelebrate.com/)
- [WintellectNOW](https://www.wintellectnow.com/Home/Instructor?instructorId=EricGreene) - Special Offer Code: GREENE-2016

### Other Resources

- [You Don't Know JS](https://github.com/getify/You-Dont-Know-JS)
- [JavaScript Air Podcast](http://javascriptair.podbean.com/)
- [Speaking JavaScript](http://speakingjs.com/es5/)
