import React, { memo } from 'react';
import PropTypes from 'prop-types';

import { carsPropType } from '../propTypes/cars';
import { ViewCarRow } from './ViewCarRow';
import { EditCarRow } from './EditCarRow';

export const CarTable = memo(({
  cars, editCarId,
  onEditCar: editCar, onDeleteCar: deleteCar,
  onSaveCar: saveCar, onCancelCar: cancelCar,
}) => {

  return <table>
    <thead>
      <tr>
        <th>Id</th>
        <th>Make</th>
        <th>Model</th>
        <th>Year</th>
        <th>Color</th>
        <th>Price</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      {cars.length === 0 && <tr><td colSpan="6">No Cars Available</td></tr>}
      {cars.map(car => car.id === editCarId
        ? <EditCarRow key={car.id} car={car} onSaveCar={saveCar} onCancelCar={cancelCar} />
        : <ViewCarRow key={car.id} car={car} onEditCar={editCar} onDeleteCar={deleteCar} />)}
    </tbody>
  </table>;
});

CarTable.defaultProps = {
  cars: [],
  editCarId: -1,
};

CarTable.propTypes = {
  cars: carsPropType,
  editCarId: PropTypes.number,
  onEditCar: PropTypes.func.isRequired,
  onDeleteCar: PropTypes.func.isRequired,
  onSaveCar: PropTypes.func.isRequired,
  onCancelCar: PropTypes.func.isRequired,
};