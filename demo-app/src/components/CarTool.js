import React, { useState, useCallback, useEffect, useRef } from 'react';

import { carsPropType } from '../propTypes/cars';
import {
  getAllCars as dbGetAllCars,
  appendCar as dbAppendCar,
  replaceCar as dbReplaceCar,
  deleteCar as dbDeleteCar,
} from '../services/cars';

import { ToolHeaderMemo } from './ToolHeader';
import { CarTable } from './CarTable';
import { CarForm } from './CarForm';

export const CarTool = ({ cars: initialCars }) => {

  const [ cars, setCars ] = useState(initialCars);
  const [ editCarId, setEditCarId ] = useState(-1);

  const defaultControl = useRef();

  const refreshCars = useCallback(() => {
    return dbGetAllCars()
      .then(cars => {
        setCars(cars);
        setEditCarId(-1);
      });
  }, []);

  useEffect(() => {
    refreshCars();

    if (defaultControl.current) {
      defaultControl.current.focus();
    }
  }, [ refreshCars ]);

  const addCar = useCallback((car) => {
    return dbAppendCar(car)
      .then(refreshCars);
  }, [ refreshCars ]);

  const replaceCar = useCallback((car) => {
    return dbReplaceCar(car)
      .then(refreshCars);
  }, [ refreshCars ]);

  const deleteCar = useCallback((carId) => {
    return dbDeleteCar(carId)
      .then(refreshCars);
  }, [ refreshCars ]);

  const cancelCar =  useCallback(() => setEditCarId(-1), []);

  return <>
    <ToolHeaderMemo headerText="Car Tool" />
    <CarTable cars={cars} editCarId={editCarId}
      onEditCar={setEditCarId} onDeleteCar={deleteCar}
      onSaveCar={replaceCar} onCancelCar={cancelCar} />
    <CarForm buttonText="Add Car" onSubmitCar={addCar} ref={defaultControl} />
  </>
};

CarTool.defaultProps = {
  cars: [],
};

CarTool.propTypes = {
  cars: carsPropType,
};
