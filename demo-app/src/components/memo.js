import React from 'react';

const same = (a, b) => {

  let areSame = true;

  if (a === b) {
    return areSame;
  }

  if (a === null || b === null) {
    return !areSame;
  }

  Object.keys(a).forEach(key => {
    if (a[key] !== b[key]) {
      areSame = false;
    }
  });

  Object.keys(b).forEach(key => {
    if (a[key] !== b[key]) {
      areSame = false;
    }
  });

  return areSame;
};

export const myMemo = Component => {

  let lastProps = null;
  let memoElement = null;

  return (props) => {

    if (!same(props, lastProps)) {
      lastProps = props;
      memoElement = <Component {...props} />;
    }

    return memoElement;

  };
  
};