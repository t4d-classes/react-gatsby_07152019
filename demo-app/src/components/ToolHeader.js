import React from 'react';
import PropTypes from 'prop-types';

import { myMemo } from './memo';

export const ToolHeader = (props) => {

  console.log('tool header rendered');

  return <header>
    <h1>{props.headerText}</h1>
  </header>;
};

export const ToolHeaderMemo = myMemo(ToolHeader);

ToolHeader.defaultProps = {
  headerText: 'The Tool',
};

ToolHeader.propTypes = {
  headerText: PropTypes.string,
};

