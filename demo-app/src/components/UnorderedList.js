import React from 'react';
import PropTypes from 'prop-types';

export const UnorderedList = (props) => {

  return <ul>
    {props.items.length === 0 &&
      <li>{props.emptyListText}</li>}
    {props.items.map( (item, index) =>
      <li key={index}>{props.children(item)}</li>)}
  </ul>;

};

UnorderedList.defaultProps = {
  items: [],
  children: item => item,
  emptyListText: 'There are no items.',
}

UnorderedList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.string),
  children: PropTypes.func,
  emptyListText: PropTypes.string,
};

