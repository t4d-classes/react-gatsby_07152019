import { useState } from 'react';

export const useForm = (initialForm) => {

  const [ form, setForm ] = useState(initialForm);

  const change = ({ target: { name, type, value } }) => {

    setForm({
      ...form,
      [ name ]: type === 'number'
        ? Number(value)
        : value,
    });
  };

  return [
    form, // form data
    change, // change function for onChange
    () => setForm(initialForm), // reset the form
  ]
};