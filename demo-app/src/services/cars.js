// export const getAllCars = () => {
//   return fetch('http://localhost:3050/cars')
//     .then(res => res.json());
// };

export const getAllCars = async () => {
  const res = await fetch('http://localhost:3050/cars');
  const cars = await res.json();
  return cars;
};

export const appendCar = car => {
  return fetch('http://localhost:3050/cars', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(car),
  })
    .then(res => res.json());
};

export const replaceCar = car => {
  return fetch('http://localhost:3050/cars/' + encodeURIComponent(car.id), {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(car),
  })
    .then(res => res.json());
};

export const deleteCar = carId => {
  return fetch('http://localhost:3050/cars/' + encodeURIComponent(carId), {
    method: 'DELETE',
  })
    .then(res => res.json());
};