const path = require('path'); 
const { createFilePath } = require(`gatsby-source-filesystem`)

exports.onCreateNode = ({ node, getNode, actions }) => {

  const { createNodeField } = actions

  if (node.internal.type === 'MarkdownRemark') {
 
    const slug = createFilePath({ node, getNode, basePath: `pages` });

    createNodeField({
      node,
      name: `slug`,
      value: slug,
    });
  }
};

exports.createPages = async ({ graphql, actions }) => {

  const { createPage } = actions;
  
  const result = await graphql(`
    query GetAllNotes {
      allNotes: allMarkdownRemark {
        notes: edges {
          note: node {
            metadata: fields {
              slug
            }
          }
        }
      }
    }
  `);

  console.log(result.data.allNotes.notes);

  result.data.allNotes.notes.forEach(({ note }) => {
    createPage({
      path: note.metadata.slug,
      component: path.resolve(`./src/templates/note.js`),
      context: {
        slug: note.metadata.slug,
        adhoc: {
          msgOfTheDay: 'Hello World!',
        },
      },
    });
  });

};