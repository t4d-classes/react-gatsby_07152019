import React from "react";
import { graphql } from 'gatsby';

import { Layout } from '../components/layout';
import { FileList } from '../components/FileList';

export default ({ data }) => (
  <Layout subTitle="Home">
    <div>Pages Folder File List</div>
    <FileList files={data.sortedNotes.notes} />
  </Layout>
);

export const query = graphql`
query {
  sortedNotes: allMarkdownRemark(
    sort: { fields: [frontmatter___title], order: ASC }, 
  ) {
    notes: nodes {
      ...MDFileInfoForList
    }
  }
}
`;
