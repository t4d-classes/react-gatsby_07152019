import React from 'react';
import { Link } from 'gatsby';

import { Layout } from '../components/layout';

export default () => {

  return <Layout subTitle="Oops...">
    <p>Page could not be found...</p>
    <Link to="/">Go Home</Link>
  </Layout>;

};