import React from 'react';
import { graphql, Link } from 'gatsby';

export const FileList = ({ files }) => {
  return <ul>
    {files.map(file => {
      return <li key={file.id}>
        <Link to={file.fields.slug}>{file.frontmatter.title} ({file.frontmatter.date})</Link>
      </li>;
    })}
  </ul>;
};

export const query = graphql`
  fragment MDFileInfoForList on MarkdownRemark {
    id
    frontmatter {
      title
      date
    }
    fields {
      slug
    }
  }
`;

