import React from 'react'
import { graphql } from "gatsby"

import { Layout } from '../components/layout'

export default (props) => {

  console.log(props);

  const { data } = props;
  const { note } = data;

  return (
    <Layout>
      <h1>{note.frontmatter.title}</h1>
      <div dangerouslySetInnerHTML={{ __html: note.html }} />
    </Layout>
  );
};


export const query = graphql`
  query($slug: String!) {
    note: markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
      }
    }
  }
`